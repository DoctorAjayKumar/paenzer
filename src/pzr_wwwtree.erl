-module(pzr_wwwtree).

-export([
    files/0
]).

% Will need to traverse all readable files, get content type and
% content length

-record(f,
        {path           = none :: none | string(),
         contents       = none :: none | binary(),
         content_type   = none :: any(),
         content_length = 0    :: non_neg_integer()}).
-type f() :: #f{}.


files() ->
    ls(www_home()).

ls(Dir) ->
    {ok, Result} = file:list_dir(Dir),
    Result.


% It's depth first recursion, not hard.
traverse([], FsAccum) ->
    lists:flatten(FsAccum);
traverse([FN

www_root() ->
    filename:join([zx:get_home(), "www"]).

